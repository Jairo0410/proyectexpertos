#include "Arduino.h"

class FotoR{
private:
  int pin;
  int limite;
public:
  FotoR(int, int limite = 50);
  int getPin() const;
  int lectura();
  bool esDia();
};

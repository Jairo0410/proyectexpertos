#include <Arduino.h>

class Alarma{

  private:
    int _pin;
  
  public:
    Alarma();
    void setPin(int pin);
    void activar();
  
};

#include "Alarma.h"

using namespace std;

Alarma::Alarma(){
  
}

void Alarma::setPin(int pin){
  this->_pin=pin;  
  pinMode(this->_pin, OUTPUT);
}

void Alarma::activar(){
  tone(_pin, 440);
  delay(1000);
  noTone(_pin);
  delay(500);
  tone(_pin, 523, 300);
  delay(500);  
  tone(_pin, 440);
  delay(1000);
  noTone(_pin);
  delay(500);
  tone(_pin, 523, 300);
  delay(500);
}

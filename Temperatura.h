#include <Arduino.h>

class Temperatura{
  
  private:
    int _pin;
    
  public:
    Temperatura(int pin);
    double getTemperatura();
    
};

#include "Led.h"

Led::Led(int pin){
  this->pin = pin;
  this->estado = LOW;
  pinMode(this->pin, OUTPUT);
}

int Led::getPin() const{
  return this->pin;
}

bool Led::estaEncendido(){
  return this->estado == HIGH;
}

void Led::encender(){
  digitalWrite(this->pin, HIGH);
}

void Led::apagar(){
  digitalWrite(this->pin, LOW);
}

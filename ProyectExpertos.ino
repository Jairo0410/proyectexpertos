#include "Temperatura.h"
#include "SensorDistancia.h"
#include "Iman.h"
#include "FotoR.h"
#include "Motor.h"
#include "Alarma.h"
#include "Led.h"

#include <SoftwareSerial.h>
SoftwareSerial BT(4,2);


// Constantes entre esclavo y maestro
#define ABRIR_PUERTA 1
#define CERRAR_PUERTA 2
#define ENCENDER_VENTILADOR 3
#define APAGAR_VENTILADOR 4
#define ENCENDER_INTERIOR 5
#define APAGAR_INTERIOR 6
#define ENCENDER_EXTERIOR 7
#define APAGAR_EXTERIOR 8
#define ENCENDER_ALARMA 9

// Datos maximos permitidos de los sensores 
#define TempMax 20
#define DistanciaMax 30
#define LuzMax 500

// Pines utilizados
#define PIN_TIMBRE 2
#define PIN_IMAN 4
#define PIN_TEMPERATURA A4
#define PIN_SALIDA_DISTANCIA 3
#define PIN_ECO_DISTANCIA A2
#define PIN_SENSOR_LUZ A3
#define PIN_PUERTA_1 11
#define PIN_PUERTA_2 12
#define PIN_VENTILADOR 10
#define PIN_ALARMA A0
#define PIN_LUZ_INTERIOR 9
#define PIN_LUZ_EXTERIOR 8


// Casa controlada por Bluethooth o no
bool automatico = false;
char accion;

// Temperatura
Temperatura* temperatura;

// Distancia
SensorDistancia* movimiento;

// Iman
Iman* iman;

// Luz
FotoR* luz;

// Salidas
Motor* ventilador;
Motor* puerta;
Led* luzInterior;
Led* luzExterior;
Alarma alarma;

void abrirPuerta(){
  Serial.println("Abriendo puerta");
  puerta->adelante();
  delay(100);
  puerta->detener();
}

void cerrarPuerta(){
  Serial.println("Cerrando puerta");
  puerta->atras();
  delay(200);
  puerta->detener();
}

void setup() {

  pinMode(PIN_VENTILADOR, OUTPUT);
  
  pinMode(PIN_TIMBRE, INPUT);

  // Pines en modo salida
  puerta = new Motor();
  puerta->setPin1(PIN_PUERTA_1);
  puerta->setPin2(PIN_PUERTA_2);
  
  ventilador = new Motor();
  ventilador->setPin1(PIN_VENTILADOR);
  ventilador->setPin2(35);

  alarma.setPin(PIN_ALARMA);

  luzInterior = new Led(PIN_LUZ_INTERIOR);
  luzExterior = new Led(PIN_LUZ_EXTERIOR);

  temperatura = new Temperatura(PIN_TEMPERATURA);
  movimiento = new SensorDistancia(PIN_SALIDA_DISTANCIA,PIN_ECO_DISTANCIA);
  iman = new Iman(PIN_IMAN);
  luz = new FotoR(PIN_SENSOR_LUZ, LuzMax);

  Serial.begin(9600);
  BT.begin(9600);
}

void mainLoop(){

  if(BT.available()>0){
    accion = BT.read();  
    if(accion == 'A')
      automatico = true;
    else {
      if(accion == 'M')
        automatico = false;  
    }
  }else{
    accion = '0';  
  }

  // Sonar alarma
  if(digitalRead(PIN_TIMBRE) == HIGH){
    //Serial.println(ENCENDER_ALARMA);
    //delay(100);
  }

  // Abrir puerta
  if(iman->isKey()){
    Serial.println(ABRIR_PUERTA);
  }

  // Si no se controla por bluethooth
  if(!automatico){

     // Encender ventilador
    bool huboMovimiento = movimiento->movimiento(DistanciaMax);
    double tempActual = temperatura->getTemperatura();
     
    if(huboMovimiento and tempActual<TempMax){
      Serial.println(ENCENDER_VENTILADOR); 
    }else {
      Serial.println(APAGAR_VENTILADOR);  
    }

    // Encender Luces
    int esDia = luz->lectura() <= LuzMax;
    
    if(esDia){
      Serial.println(APAGAR_INTERIOR);
      Serial.println(APAGAR_EXTERIOR);
    }else {
        Serial.println(ENCENDER_EXTERIOR);
        if(movimiento->movimiento(DistanciaMax)){
          Serial.println(ENCENDER_INTERIOR);
        }else{
            Serial.println(APAGAR_INTERIOR);
        }
    }

  }else{
      switch(accion){
        case char(ABRIR_PUERTA):
          abrirPuerta();
          Serial.println("Abriendo puerta");
          break;
        case char(CERRAR_PUERTA):
          cerrarPuerta();
          Serial.println("Cerrando puerta");
          break;
        case char(ENCENDER_VENTILADOR):
          ventilador->adelante();
          Serial.println("Encendiendo ventilador");
          break;
        case char(APAGAR_VENTILADOR):
          ventilador->detener();
          Serial.println("Apagando ventilador");
          break;
        case char(ENCENDER_INTERIOR):
          Serial.println("Encendiendo luces internas");
          break;
        case char(APAGAR_INTERIOR):
          Serial.println("Apagando luces internas");
          break;
        case char(ENCENDER_EXTERIOR):
          Serial.println("Encendiendo luces externas");
          break;
        case char(APAGAR_EXTERIOR):
          Serial.println("Apagando luces internas");
          break;
      }
    }
}

void loop() {
  mainLoop();
  delay(500);
}

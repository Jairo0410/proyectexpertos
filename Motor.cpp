#include "Motor.h"

Motor::Motor(){
  
} // Motor

void Motor::setPin1(int pin){
  this->pin1=pin;
  pinMode(this->pin1, OUTPUT);
} // setPin1

void Motor::setPin2(int pin){
  this->pin2=pin;
  pinMode(this->pin2, OUTPUT);
} // setPin2

void Motor::adelante(){
  digitalWrite(this->pin2,LOW);
  digitalWrite(this->pin1,HIGH);
} // adelante

void Motor::atras(){
  digitalWrite(this->pin1,LOW);
  digitalWrite(this->pin2,HIGH);
} // atras

void Motor::detener(){
  digitalWrite(this->pin1,LOW);
  digitalWrite(this->pin2,LOW);
} // alto

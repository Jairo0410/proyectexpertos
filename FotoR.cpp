#include "FotoR.h"

FotoR::FotoR(int pin, int limite){
  this->pin = pin;
  this->limite = limite;
  pinMode(this->pin, INPUT);
}

int FotoR::getPin() const{
  return this->pin;
}

bool FotoR::esDia(){
  int light = analogRead(this->pin);
  return light <= this->limite;
}

int FotoR::lectura(){
  return analogRead(this->pin);
}

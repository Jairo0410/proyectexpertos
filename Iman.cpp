#include "Iman.h"

Iman::Iman(int pin){
  this->_pin=pin;
  pinMode(this->_pin, INPUT);
}

bool Iman::isKey(){
  if(digitalRead(this->_pin) == HIGH){
    return true;  
  }  
  return false;
}

#include <Arduino.h>

class Motor{

  public:
    Motor();
    void setPin1(int pin);
    void setPin2(int pin);

    void adelante();
    void atras();
    void detener();

  private:
    int pin1;
    int pin2;   
};

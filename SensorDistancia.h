#include <Arduino.h>

class SensorDistancia{

  public:
    SensorDistancia(int salida, int eco);
    bool movimiento(int alcance);
    long lectura();

  private:
    int salida;
    int eco;

};

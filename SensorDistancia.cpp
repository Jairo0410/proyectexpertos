#include "SensorDistancia.h"

SensorDistancia::SensorDistancia(int salida, int eco){
  this->salida=salida;
  pinMode(this->salida, OUTPUT);

  this->eco=eco;
  pinMode(this->eco, INPUT);
} // SensorDistancia

long SensorDistancia::lectura(){
  digitalWrite(this->salida,LOW);
  delayMicroseconds(5);
  digitalWrite(this->salida, HIGH);
  delayMicroseconds(10);
  long tiempo=pulseIn(this->eco, HIGH);
  long distancia= int(0.017*tiempo);

  return distancia;
}

bool SensorDistancia::movimiento(int alcance = 40){
  long distancia = this->lectura();
  return distancia<=alcance;
} // objetoAlFrente

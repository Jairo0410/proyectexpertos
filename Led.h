#include "Arduino.h"

class Led{
private:
  int pin;
  int estado;
public:
  Led(int);
  int getPin() const;
  void apagar();
  void encender();
  bool estaEncendido();
};
